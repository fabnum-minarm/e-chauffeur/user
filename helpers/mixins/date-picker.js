import { DateTime } from 'luxon';

export default () => ({
  methods: {
    hourReservationScope(date) {
      const now = DateTime.local();
      const formattedDate = DateTime.fromJSDate(date);
      const { hour: dateHour } = formattedDate;
      let isOutOfScope = false;

      if (this.campus.workedHours) {
        const { start, end } = this.campus.workedHours;
        isOutOfScope = dateHour < start || dateHour > end;
      }

      if (!isOutOfScope && now.startOf('day').toISO() === formattedDate.startOf('day').toISO()) {
        isOutOfScope = dateHour < now.hour;
      }

      return isOutOfScope;
    },
    dayReservationScope(date) {
      const formattedDate = DateTime.fromJSDate(date);
      const today = DateTime.local().startOf('day');

      if (formattedDate < today) {
        return true;
      }

      if (this.campus) {
        const maxReservationScope = today.plus({ seconds: this.campus.defaultReservationScope });
        return formattedDate > maxReservationScope;
      }

      return false;
    },
  },
});
