/* eslint-disable no-param-reassign */

export const PASSENGERS_STATUSES = {
  BOOKED: 'booked',
  WARNED: 'warned',
  IN: 'in',
  OUT: 'out',
};

export const state = () => ({
  factory: {},
  type: null,
});

export const getters = {
  factory: (s) => s.factory,
};

export const mutations = {
  setFactory(s, factory) {
    s.factory = factory;
  },
};
