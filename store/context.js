/* eslint-disable no-param-reassign */
export function generateTitles(title, subtitle) {
  return { title, subtitle };
}

export function generatePreviousLink(name, params) {
  return { name, params };
}

export const state = () => ({
  title: '',
  subtitle: '',
});

export const getters = {
  title: (s) => s.title,
  subtitle: (s) => s.subtitle,
};

export const mutations = {
  setTitle(s, title) {
    s.title = title;
  },
  setSubtitle(s, subtitle) {
    s.subtitle = subtitle;
  },
};

export const actions = {
  setTitles({ commit }, { title = '', subtitle = '' } = {}) {
    commit('setTitle', title);
    commit('setSubtitle', subtitle);
  },
};
