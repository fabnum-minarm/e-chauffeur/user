import {
  CREATED, VALIDATED, ACCEPTED, STARTED, WAITING, IN_PROGRESS,
} from '@fabnumdef/e-chauffeur_lib-vue/api/status/states';
import { CANCELED_STATUSES } from '@fabnumdef/e-chauffeur_lib-vue/api/status';
import { DateTime } from 'luxon';
import { PASSENGERS_STATUSES } from '~/store/shuttle';

export default (type) => ({
  computed: {
    month() {
      return DateTime.fromISO(this[type].start).toLocaleString({ month: 'long' });
    },
    day() {
      return DateTime.fromISO(this[type].start).toLocaleString({ weekday: 'long', day: '2-digit' });
    },
    hour() {
      return DateTime.fromISO(this[type].start).toLocaleString({ hour: '2-digit', minute: '2-digit' });
    },
  },
  methods: {
    translateStatus(status) {
      if (CANCELED_STATUSES.includes(status)) {
        return 'Annulée';
      }

      switch (status) {
        case CREATED:
          return 'Créée';
        case VALIDATED:
          return 'Validée';
        case ACCEPTED:
          return 'Acceptée';
        case STARTED:
          return 'Commencée';
        case WAITING:
          return 'En attente';
        case IN_PROGRESS:
          return 'En cours';
        case PASSENGERS_STATUSES.BOOKED:
          return 'Place réservée';
        case PASSENGERS_STATUSES.WARNED:
          return 'Navette à l\'arrêt précédent';
        default:
          return '';
      }
    },
  },
});
