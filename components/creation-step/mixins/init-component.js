export default (
  titles = null,
) => ({
  async mounted() {
    await this.$store.dispatch('context/setTitles', titles);
  },
});
