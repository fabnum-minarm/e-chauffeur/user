import Vue from 'vue';
import Datetime from 'vue2-datepicker';
import 'vue2-datepicker/index.css';

Vue.component('DateTime', Datetime);
