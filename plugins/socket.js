import { DELIVERED } from '@fabnumdef/e-chauffeur_lib-vue/api/status/states';
import { CANCELED_STATUSES } from '@fabnumdef/e-chauffeur_lib-vue/api/status';

export default function socketPlugin({ store, app }) {
  const ioInstance = app.$io;
  if (ioInstance) {
    ioInstance.on('connect', () => {
      ioInstance.emit(
        'roomJoinRide',
        { id: store.getters['displacement/displacement'].id, token: store.getters['displacement/displacement'].token },
      );
    });
    ioInstance.on('rideUpdate', (ride) => {
      if (ride && (ride.status === DELIVERED || CANCELED_STATUSES.indexOf(ride.status) !== -1)) {
        ioInstance.close();
      }
    });

    const autoConnect = (ride) => {
      if (ride && ride.id && ride.status !== DELIVERED && CANCELED_STATUSES.indexOf(ride.status) === -1) {
        ioInstance.open();
      } else {
        ioInstance.close();
      }
    };
    autoConnect(store.getters['displacement/displacement']);
    store.watch((state, getters) => getters['displacement/displacement'], autoConnect);
  }
}
